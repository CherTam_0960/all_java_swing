/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.java_swing;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;

/**
 *
 * @author kitti
 */
public class Swing_JSlider extends JFrame {

    public Swing_JSlider() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        slider.setMinorTickSpacing(2);
        slider.setMajorTickSpacing(10);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);

        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String s[]) {
        Swing_JSlider frame = new Swing_JSlider();
        frame.pack();
        frame.setVisible(true);
    }
}
