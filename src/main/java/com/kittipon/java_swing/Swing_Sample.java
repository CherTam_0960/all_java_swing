/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.java_swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author kitti
 */
public class Swing_Sample {

    public static void main(String[] args) {
        JFrame f = new JFrame();//creating instance of JFrame  

        JButton b = new JButton("click");//creating instance of JButton
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        b.setBounds(130, 100, 100, 40);//x axis, y axis, width, height  

        f.add(b);//adding button in JFrame  

        f.setSize(400, 500);//400 width and 500 height  
        f.setLayout(null);//using no layout managers  
        f.setVisible(true);//making the frame visible

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Click!!!");
            }
        });
    }

}
